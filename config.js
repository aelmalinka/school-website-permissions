'use strict';

const fs = require('fs');

function merge(a, b) {
	if(typeof b === 'undefined' || a && typeof a !== 'object' && !Array.isArray(a))
		return a;
	if(!a && b && typeof b !== 'object' && !Array.isArray(b))
		return b;

	const result = {};

	for(const k in a) {
		result[k] = merge(a[k], b[k]);
	}
	for(const k in b) {
		result[k] = merge(a[k], b[k]);
	}

	return result;
}

const HTTP_ALT_PORT = 8080;
const DB_DEFAULT = 'website';

const defaults = merge(
	// 2019-07-31 AMR TODO: is it possible to do this without sync?
	fs.existsSync('./config.json') ? JSON.parse(fs.readFileSync('./config.json')) : {},
	{
		server: {
			port: HTTP_ALT_PORT
		},
		database: {
			database: DB_DEFAULT
		}
	}
);

const overrides = {
	server: {
		port: process.env.PORT,
	},
	database: {
		database: process.env.PGDATABASE,
		user: process.env.PGUSER,
		password: process.env.PGPASSWORD
	}
};

module.exports = merge(overrides, defaults);
