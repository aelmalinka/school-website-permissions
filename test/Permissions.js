const assert = require('assert');
const Permissions = require('../index.js');
const { Pool } = require('pg');
const config = require('../config.js');
const http = require('http');

describe('Permissions', function() {
	const port = 9093;
	const pool = new Pool(config.database);

	function request(page, options, done, cb) {
		return http.request(`http://localhost:${port}${page}`, options, (res) => {
			if(typeof cb !== 'undefined')
				cb(res);
		}).on('error', (e) => {
			done(e);
		});
	}
	function parse(res, done, cb) {
		res.body = [];
		res.on('data', (chunk) => {
			res.body.push(chunk)
		}).on('end', () => {
			res.body = Buffer.concat(res.body).toString()				
			if(res.headers['content-type'] === 'application/json')
				res.body = JSON.parse(res.body);
			cb(res);
		}).on('error', (e) => {
			done(e);
		});
	}
	function get(page, u, done, cb) {
		const options = {
			method: 'GET',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end();
	}
	function del(page, u, done, cb) {
		const options = {
			method: 'DELETE',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end();
	}
	function put(page, u, data, done, cb) {
		const options = {
			method: 'PUT',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end(data);
	}
	function post(page, u, data, done, cb) {
		const options = {
			method: 'POST',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end(data);
	}

	const user = {
		name: 'aladdin',
		pass: 'open sesame',
		key: '',
		get auth() {
			return `${this.name}:${this.key}`;
		},
		get header() {
			return `Basic ${Buffer.from(this.auth).toString('base64')}`;
		}
	}
	const user_forbidden = {
		name: 'abu',
		pass: 'eee eee eee ooo ooo ooo',
		key: '',
		get auth() {
			return `${this.name}:${this.key}`;
		},
		get header() {
			return `Basic ${Buffer.from(this.auth).toString('base64')}`;
		}
	};
	
	before(function(done) {
		Permissions.close(() => {
			Permissions.listen(port, () => {
				done();
			});
		});
	});
	after(function(done) {
		Permissions.close(() => {
			done();
		});
	});
	
	it('should allow accessing all permissions', function(done) {
		get('/all', null, done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 200);
				assert.equal(typeof res.body, 'object');
				done()
			});
		});
	});
	it.skip('should tell a user their permissions', function(done) {
		get('/permission', user, done, (res) => {
			parse(res, done, (res) => {
				assert.equal(res.statusCode, 200);
				assert.equal(typeof res.body, 'object');
				assert.equal(typeof res.body.permissions, 'object');
				assert.equal(typeof res.body.permissions.users, 'object');
				assert(res.body.permissions.users.add);
				assert(res.body.permissions.users.edit);
				assert(res.body.permissions.users.remove);
				done();
			});
		});
	});
});