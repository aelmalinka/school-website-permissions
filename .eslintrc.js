module.exports = {
	'env': {
		'commonjs': true,
		'es6': true,
		'node': true
	},
	'extends': 'eslint:recommended',
	'globals': {
		'Atomics': 'readonly',
		'SharedArrayBuffer': 'readonly'
	},
	'parserOptions': {
		'ecmaVersion': 2018
	},
	'rules': {
		'indent': [
			'error',
			'tab',
			{
				'SwitchCase': 1
			},
		],
		'linebreak-style': [
			'error',
			'unix'
		],
		'quotes': [
			'error',
			'single'
		],
		'semi': [
			'error',
			'always'
		],
		'no-await-in-loop': 'error',
		'no-console': 'error',
		'no-extra-parens': 'error',
		'no-template-curly-in-string': 'error',
		'no-else-return': 'error',
		'no-eq-null': 'error',
		'no-extra-bind': 'error',
		'no-fallthrough': 'error',
		'no-implicit-coercion': 'error',
		'no-implied-eval': 'error',
		'no-invalid-this': 'error',
		'no-labels': 'error',
		'no-lone-blocks': 'error',
		'no-loop-func': 'error',
		'no-multi-spaces': 'error',
		'no-new': 'error',
		'no-new-func': 'error',
		'no-new-wrappers': 'error',
		'no-proto': 'error',
		'no-restricted-properties': 'error',
		'no-return-assign': 'error',
		'no-return-await': 'error',
		'no-script-url': 'error',
		'no-self-compare': 'error',
		'no-unused-expressions': 'error',
		'no-useless-call': 'error',
		'no-useless-concat': 'error',
		'no-useless-return': 'error',
		'vars-on-top': 'error',
		'wrap-iife': 'error',
		'yoda': [
			'error',
			'never',
		],
		'strict': [
			'error',
			'global',
		],
		'init-declarations': 'error',
		'no-shadow': 'error',
		'no-undef-init': 'error',
		'no-undefined': 'error',
		'no-use-before-define': 'error',
		'global-require': 'error',
		'no-mixed-requires': 'error',
		'no-buffer-constructor': 'error',
		'handle-callback-err': 'error',
		'no-path-concat': 'error',
		'no-process-exit': 'error',
		'no-sync': 'warn',
		'consistent-this': 'error',
		'func-names': 'error',
		'no-array-constructor': 'error',
		'no-continue': 'error',
		'no-lonely-if': 'error',
		'no-multiple-empty-lines': 'error',
		'no-trailing-spaces': 'error',
		'no-underscore-dangle': [
			'error',
			{
				'allowAfterThis': true,
			},
		],
		'no-whitespace-before-property': 'error',
		'arrow-body-style': 'error',
		'arrow-parens': 'error',
		'arrow-spacing': 'error',
		'no-confusing-arrow': 'error',
		'no-duplicate-imports': 'error',
		'no-useless-constructor': 'error',
		'no-var': 'error',
		'prefer-const': 'error',
	}
};