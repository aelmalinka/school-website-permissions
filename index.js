'use strict';

const {
	Application,
	NotAuthenticated,
	NotAuthorized,
	NotFound,
} = require('Application');
const { User } = require('User');
const { Pool } = require('pg');
const config = require('./config.js');
const debug = require('debug')('http');

const app = module.exports = new Application();

const permissions = {
	get: async (ctx) => {
		let user = null;
		if(!ctx.user.isLoggedIn)
			throw new NotAuthenticated();
		if(ctx.req.url !== '/' && !(await ctx.user.getPermission(ctx.pool)).users.view)
			throw new NotAuthorized();
		else if(ctx.req.url === '/')
			user = ctx.user;
		else
			user = new User(ctx.req.url.split('/')[1]);
		if(!await user.isValid(ctx.pool))
			throw new NotFound();

		ctx.res.body = {
			name: user.name,
			permissions: await user.getPermission(ctx.pool),
		};
	},
	def: async (ctx) => {
		if(!(await ctx.user.getPermission(ctx.pool)).permissions['default'])
			throw new NotAuthorized();
		const {
			rowCount,
			rows
		} = await ctx.pool.query(
			'SELECT permissions FROM users.permission WHERE name = $1::text',
			['default']
		);
		if(rowCount == 0)
			throw new NotFound();
		else if (rowCount > 1)
			throw new Error('multiple rows returned for default permissions');

		ctx.res.body = rows[0];
	},
	all: async (ctx) => {
		if(!(await ctx.user.getPermission(ctx.pool)).permissions.all)
			throw new NotAuthorized();
		const {
			rowCount,
			rows
		} = await ctx.pool.query(
			'SELECT jsonb_object_agg(obj.app, obj.perms) AS value FROM (' +
			'SELECT keys.app, jsonb_agg(keys.key) as perms FROM (' +
			'SELECT DISTINCT app, key FROM users.permissions, LATERAL jsonb_each(data) ORDER BY app ' +
			') keys ' +
			'GROUP BY app ' +
			') obj'
		);
		if(rowCount == 0)
			throw new NotFound();
		else if(rowCount > 1)
			throw new Error('multiple rows returned for all permissions');

		ctx.res.body = rows[0].value;
	},
};

app.use(async (ctx) => {
	ctx.pool = new Pool(config.database);
});
app.use(async (ctx) => {
	ctx.user = new User();

	if(
		typeof ctx.req.headers['authorization'] !== 'undefined' &&
		!await ctx.user.check(ctx)
	)
		throw new NotAuthenticated();
});
app.use(async (ctx, next) => {
	await next();
	debug(`${ctx.req.method} ${ctx.req.url} ${ctx.res.code}`);
});
app.use(async (ctx, next) => {
	await next();

	switch(ctx.req.method) {
		case 'GET':
			if(ctx.req.url == '/default')
				await permissions.def(ctx);
			else if(ctx.req.url == '/all')
				await permissions.all(ctx);
			else
				await permissions.get(ctx);
			break;
	}
});

app.listen(config.server.port, () => {
	debug(`listening on http://0.0.0.0:${config.server.port}/`);
});
